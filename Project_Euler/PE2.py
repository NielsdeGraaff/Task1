a, b, result = 1, 2, 0

while b <= 4000000:
    if not b % 2:
        result += b
    a, b = b, a + b

print(result)
