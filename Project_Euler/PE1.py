
max_amount = 1000
result = 0

for x in range(max_amount):
    if x % 3 == 0 or x % 5 == 0:
        result += x

print("result: " + str(result))

